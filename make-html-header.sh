#!/bin/bash

rm -f src/html.h

pushd src/
for f in `ls *.html`; do
  cat $f | sed 's/"/\\"/g' | sed -E 's/^(.*)$/"\1" \\/' | \
  sed "1s/^/#include <Arduino.h>\n\\/\\/ This file is generated automatically. Do not edit!\n\nstatic const String $(echo $f | sed "s/[.]/_/") = \\\\\n/" |
  sed '$ s/ \\/;\n/' >> html.h
done
popd
